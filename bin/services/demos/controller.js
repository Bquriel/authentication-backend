

// /v1/demos/access-auth => GET
exports.getAccessAuth = async ( req, res ) => {
  res.code( 200 ).send({ message: "You made it!", "required-to-make-here": "to be authenticated", "your-role": req.token?.user?.role });
}

// /v1/demos/access-regular => GET
exports.getAccessRegular = async ( req, res ) => {
  res.code( 200 ).send({ message: "You made it!","required-to-make-here": "to be regular", "your-role": req.token?.user?.role });
}

// /v1/demos/access-editor => GET
exports.getAccessEditor = async ( req, res ) => {
  res.code( 200 ).send({ message: "You made it!","required-to-make-here": "to be editor", "your-role": req.token?.user?.role });
}

// /v1/demos/access-admin => GET
exports.getAccessAdmin = async ( req, res ) => {
  res.code( 200 ).send({ message: "You made it!","required-to-make-here": "to be admin", "your-role": req.token?.user?.role });
}

// /v1/demos/access-super-admin => GET
exports.getAccessSuperAdmin = async ( req, res ) => {
  res.code( 200 ).send({ message: "You made it!","required-to-make-here": "to be super admin", "your-role": req.token?.user?.role });
}