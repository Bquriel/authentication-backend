const { join }    = require( "path" );

const controller  = require( join( __dirname, "controller" ));


module.exports = async ( fastify, options ) => {
  /****************************** Routes ******************************/
  // /v1/demos/access-auth => GET
  fastify.get( "/access-auth",          { onRequest: fastify.isAuth },                                  controller.getAccessAuth );

  // /v1/demos/access-regular => GET
  fastify.get( "/access-regular",       { onRequest: fastify.restrict( __types.roles.REGULAR )},        controller.getAccessRegular );

  // /v1/demos/access-editor => GET
  fastify.get( "/access-editor",        { onRequest: fastify.restrict( __types.roles.EDITOR )},         controller.getAccessEditor );

  // /v1/demos/access-admin => GET
  fastify.get( "/access-admin",         { onRequest: fastify.restrict( __types.roles.ADMIN )},          controller.getAccessAdmin );

  // /v1/demos/access-super-admin => GET
  fastify.get( "/access-super-admin",   { onRequest: fastify.restrict( __types.roles.SUPER_ADMIN )},    controller.getAccessSuperAdmin );
  /****************************** !Routes ******************************/
};