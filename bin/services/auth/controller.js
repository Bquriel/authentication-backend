const { join }  	= require( "path" );

const bcrypt     	= require( "bcryptjs" );

const User       	= require( join( __rootPath, "models", "user" ));
const Settings   	= require( join( __rootPath, "models", "settings" ));


const hashRounds = +__accessEnv( "AUTHENTICATION_PASSWORD_HASH_ROUNDS" );


// /v1/auth/register => POST
exports.postRegister = async ( req, res ) => {
	const user = req.body;

	try {
		const oldUser = await User.findOne({ email: user.email }).select( "_id" ).lean();

		if( oldUser ){
			return res.code( 422 ).send({ message: "User with this email already exists" });
		} else if( user.password !== user.confirmPassword ){
			return res.code( 400 ).send({ message: "Passwords don't match" });
		}

		user.password = await bcrypt.hash( user.password, hashRounds );
		let newUser;

		const session = await req.mongoose.startSession();
		await session.withTransaction( async () => {
      [ newUser ]             = await User.create([ user ], { session });

      const [ settings ]      = await Settings.create([{ user: newUser._id }], { session });

      newUser._doc.settings   = settings;
    });

    delete newUser._doc.password;
    const payload = { user: { ...newUser._doc, _id: newUser._id.toString()}};

    const token   = await req.signJWT( req, res, payload );

    // Call to email service can be added here.

		res.code( 200 ).send({ token, user: newUser });
	} catch( err ){
		req.handleError( err, "Error in registering" );
	}
}

// /v1/auth/login => POST
exports.postLogin = async ( req, res ) => {
  const email     = req.body.email;
  const password  = req.body.password;

	try {
    const user = await User.findOne({ email }).select( "_id name email role password" ).lean();

    if( !user ){
      return res.code( 401 ).send({ message: "Incorrect email or password" });
    }

    const match = await bcrypt.compare( password, user.password );

    if( !match ){
      return res.code( 401 ).send({ message: "Incorrect email or password" });
    }

    user.settings = await Settings.findOne({ user: user._id }).select( "-createdAt -updatedAt -user" ).lean();

    delete user.password;

    const payload = { user: { ...user, _id: user._id.toString()}};
    const token   = await req.signJWT( req, res, payload );

		res.code( 200 ).send({ user, token });
	} catch( err ){
		req.handleError( err, "Error in logging in" );
	}
}

// /v1/auth/logout => POST
exports.postLogout = async ( req, res ) => {
	try {
    res.code( 204 ).clearCookie( "authorization" );
	} catch( err ){
		req.handleError( err, "Error in logging out" );
	}
}

// /v1/auth/verify => POST
exports.postVerify = async ( req, res ) => {
  const token = req.body.token;

	try {
    if( !token ){
      return res.code( 400 ).send({ message: "'token' is required as part of the body" });
    }
    const decodedToken = await req.verifyJWT( token );

    const response = { valid: decodedToken ? true : false };
    if( decodedToken ){
      response.exp          = new Date( decodedToken.exp * 1000 );
      response.decodedToken = decodedToken;
    }

		res.code( 200 ).send( response );
	} catch( err ){
		req.handleError( err, "Error in verifying token" );
	}
}