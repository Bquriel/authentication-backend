const { join }    = require( "path" );

const controller  = require( join( __dirname, "controller" ));

module.exports = async ( fastify, options ) => {
  /****************************** Routes ******************************/
  // /v1/auth/register => POST
  fastify.post( "/register",                    controller.postRegister );

  // /v1/auth/login => POST
  fastify.post( "/login",                       controller.postLogin );

  // /v1/auth/logout => POST
  fastify.post( "/logout",                      controller.postLogout );

  // /v1/auth/verify => POST
  fastify.post( "/verify",                      controller.postVerify );
  /****************************** !Routes ******************************/
};