const { join }    = require( "path" );

const User        = require( join( __rootPath, "models", "user" ));
const Settings   	= require( join( __rootPath, "models", "settings" ));


// /v1/users => GET
exports.getUsers = async ( req, res ) => {
  const fields = req.query.fields;

  let select = "-password -resetToken -resetTokenExpiration";
  if( fields ){
    const illegalFields = [ "password", "resetToken", "resetTokenExpiration" ];
    select = fields.split( "," ).filter( item => !illegalFields.includes( item )).join( " " );
  }

  try {
    const users = await User.find().select( select ).lean();

    res.code( 200 ).send({ users });
  } catch( err ){
    req.handleError( err, "Error in getting users" );
  }
}

// /v1/users/:userId => GET
exports.getUser = async ( req, res ) => {
  const id = req.params.userId;

  try {
    const user = await User.findById( id ).select( "-password -resetToken -resetTokenExpiration" ).lean();

    if( !user ){
      return res.code( 404 ).send({ message: "User not found" });
    }

    res.code( 200 ).send({ user });
  } catch( err ){
    if( err instanceof req.mongoose.Error.CastError ){
      return res.code( 400 ).send({ message: "Incorrect userId" });
    }
    req.handleError( err, "Error in getting users" );
  }
}

// /v1/users => POST
exports.postAddUser = async ( req, res ) => {
  const user = req.body;

  try {
    let newUser;

		const session = await req.mongoose.startSession();
		await session.withTransaction( async () => {
      [ newUser ]             = await User.create([ user ], { session });

      const [ settings ]      = await Settings.create([{ user: newUser._id }], { session });

      newUser._doc.settings   = settings;
    });

    res.code( 201 ).send({ user: newUser });
  } catch( err ){
    req.handleError( err, "Error in creating user" );
  }
}

// /v1/users => PUT
exports.putEditUser = async ( req, res ) => {
  const user = req.body;
  
  // These fields should not be edited.
  delete user.password;
  delete user.resetToken;
  delete user.resetTokenExpiration;

  try {
    const updatedUser = await User.findByIdAndUpdate( user._id, user, { new: true }).lean();

    res.code( 200 ).send({ user: updatedUser });
  } catch( err ){
    if( err instanceof req.mongoose.Error.CastError ){
      return res.code( 400 ).send({ message: "Incorrect userId" });
    }
    req.handleError( err, "Error in getting users" );
  }
}

// /v1/users => DELETE
exports.deleteUser = async ( req, res ) => {
  const id = req.body.id;

  try {
    const user = await User.findByIdAndRemove( id ).select( "-password -resetToken -resetTokenExpiration" ).lean();

    if( !user ){
      return res.code( 404 ).send({ message: "User not found" });
    }

    res.code( 200 ).send({ user });
  } catch( err ){
    if( err instanceof req.mongoose.Error.CastError ){
      return res.code( 400 ).send({ message: "Incorrect userId" });
    }
    req.handleError( err, "Error in getting users" );
  }
}