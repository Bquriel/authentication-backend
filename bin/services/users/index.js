const { join }    = require( "path" );

const controller  = require( join( __dirname, "controller" ));


module.exports = async ( fastify, options ) => {
  // Response object.
  const userSchema = {
    type: "object",
    properties: {
      _id: { type: "string" },
      name: { type: "string" },
      email: { type: "string" },
      role: { type: "string" },
      createdAt: { type: "string" },
      updatedAt: { type: "string" }
    }
  };

  const userArray = {
    type: "object",
    properties: {
      users: {
        type: "array",
        items: userSchema
      }
    }
  };

  const opts = {
    onRequest: fastify.restrict( __types.roles.EDITOR ),
    schema: {
      response: {
        200: {
          type: "object",
          properties: {
            user: userSchema
          }
        }
      }
    }
  };

  const arrayOpts = {
    onRequest: fastify.restrict( __types.roles.EDITOR ),
    schema: {
      response: {
        200: userArray
      }
    }
  };

  /****************************** Routes ******************************/
  // /v1/users => GET
  fastify.get( "/",           arrayOpts,              controller.getUsers );

  // /v1/users/:userId => GET
  fastify.get( "/:userId",    opts,                   controller.getUser );

  // /v1/users => POST
  fastify.post( "/",          opts,                   controller.postAddUser );

  // /v1/users => PUT
  fastify.put( "/",           opts,                   controller.putEditUser );

  // /v1/users => DELETE
  fastify.delete( "/",        opts,                   controller.deleteUser );
  /****************************** !Routes ******************************/
};