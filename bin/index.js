// Core node packages.
require( "events" ).EventEmitter.defaultMaxListeners = 25;
const { join }        = require( "path" );

// Globals.
global.__rootPath     = __dirname;
global.__accessEnv    = require( join( __rootPath, "util", "accessEnv" ));
global.__types        = require( join( __rootPath, "types" ));

const isDev = __accessEnv( "NODE_ENV" ) === "development";
if( __accessEnv( "CONTEXT", null ) === "localhost" ){
  try {
    const envfile = __accessEnv( "ENVFILE" );
    require( "dotenv" ).config({ path: join( __dirname, envfile )});
  } catch( err ){
    fastify.log.error( "ERROR, environment variable ENVFILE must be specified when running localhost!" );
    process.exit( 1 );
  }
}
const logger = { level: "error" };
if( isDev ){
  logger.level = "info";
  logger.prettyPrint = { colorize: true };
}

// Third party packages.
const fastify         = require( "fastify" )({ logger, trustProxy: true });
const helmet          = require( "fastify-helmet" );
const mongoose        = require( "mongoose" );
const fastifyCookie   = require( "fastify-cookie" );


// Own packages.
const allowCORS       = require( join( __rootPath, "plugins", "allowCORS" ));
const JWT             = require( join( __rootPath, "plugins", "JWT" ));
const error           = require( join( __rootPath, "plugins", "error" ));
const auth            = require( join( __rootPath, "plugins", "auth" ));
const authService     = require( join( __rootPath, "services", "auth" ));
const userService     = require( join( __rootPath, "services", "users" ));
const demosService    = require( join( __rootPath, "services", "demos" ));


// Decorators.
fastify.decorateRequest( "mongoose", mongoose );

// Plugins.
fastify.register( fastifyCookie, { secret: __accessEnv( "COOKIE_SECRET" )});
fastify.register( allowCORS );
fastify.register( helmet );
fastify.register( error );
fastify.register( JWT );
fastify.register( auth );

// Services.
fastify.register( authService,  { prefix: "/v1/auth" });
fastify.register( userService,  { prefix: "/v1/users" });
fastify.register( demosService, { prefix: "/v1/demos" });


const db = mongoose.connection;

db.on( "connecting", () => {
  fastify.log.info( "[SERVER] Connecting to database.." );
});

db.on( "error", err => {
  fastify.log.error( "Error in MongoDb connection", err );
  mongoose.disconnect();
});

db.on( "disconnected", async () => {
  fastify.log.error( "[SERVER] MongoDB disconnected!" );
  process.exit( 1 );
});

const dbUrl = __accessEnv( "MONGODB_URI" );

( async () => {
  try {
    const port = +__accessEnv( "PORT", 9012 );

    await mongoose.connect( dbUrl, {
      useNewUrlParser: true,
      poolSize: 15,
      useFindAndModify: false,
      useUnifiedTopology: true,
      useCreateIndex: true
    });

    const address = await fastify.listen( port, "::" );

    fastify.log.info( `Server listening on ${ address }` );
  } catch ( err ){
    fastify.log.error( err );
    process.exit( 1 );
  }
})();