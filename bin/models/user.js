const mongoose = require( "mongoose" );

const schema = new mongoose.Schema({
    name: { type: String, required: true },
    email: { type: String, required: true, unique: true },
    role: { type: String, enum: Object.values( __types.roles ), default: __types.roles.REGULAR },
    password: { type: String, required: true },
    resetToken: { type: String, index: true },
    resetTokenExpiration: Date
}, { timestamps: true, autoIndex: true, autoCreate: true });

const model =  mongoose.model( "User", schema );

module.exports = model;