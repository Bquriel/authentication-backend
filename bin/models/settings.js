const mongoose = require( "mongoose" );

const schema = new mongoose.Schema({
    user: { type: mongoose.Types.ObjectId, required: true, index: true },
    someSettingsValue: { type: String, required: true, default: "somevalue" }
}, { timestamps: true, autoIndex: true, autoCreate: true });

const model =  mongoose.model( "Settings", schema );

module.exports = model;