exports.roles = {
    REGULAR:        "regular",
    EDITOR:         "editor",
    ADMIN:          "admin",
    SUPER_ADMIN:    "super admin"
};