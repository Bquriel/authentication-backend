const { join }      = require( "path" );

const fastifyPlugin = require( "fastify-plugin" );

const { roles }     = require( join( __rootPath, "types" ));


const isAuthenticated = ( req ) => {
  if( !req.token?.user )
    return false;

  req.isAuthenticated = true;
  const accessLevel   = req.token.user.role;

  req.isSuperAdmin    = accessLevel === roles.SUPER_ADMIN
  req.isAdmin         = accessLevel === roles.ADMIN    || req.isSuperAdmin;
  req.isEditor        = accessLevel === roles.EDITOR   || req.isAdmin;
  req.isRegular       = accessLevel === roles.REGULAR  || req.isEditor;

  return true;
}

const isAuth = ( req, res, next ) => {
  if( !isAuthenticated( req ))
    res.code( 401 ).send({ message: "Not authenticated" });
  next();
}

const restrict = authLevel => ( req, res, next ) => {
  if( !authLevel ){
    throw new Error( "Required auth level must be provided!" );
  } else if( !Object.values( roles ).includes( authLevel )){
    throw new Error( "Argument must be 'regular', 'editor', 'admin', or 'super admin'!" );
  }
  if( !isAuthenticated( req, res ))
    res.code( 401 ).send({ message: "Not authenticated" });

  if(( authLevel === roles.SUPER_ADMIN && !req.isSuperAdmin ) || ( authLevel === roles.ADMIN && !req.isAdmin ) || ( authLevel === roles.EDITOR && !req.isEditor ) || ( authLevel === roles.REGULAR && !req.isRegular )){
    res.code( 403 ).send({ message: "Forbidden" });
  }

  next();
}

// Plugin for authentication.
module.exports = fastifyPlugin(( instance, options, next ) => {
  instance.decorateRequest( "isAuthenticated", false );
  instance.decorateRequest( "isRegular", false );
  instance.decorateRequest( "isEditor", false );
  instance.decorateRequest( "isAdmin", false );
  instance.decorateRequest( "isSuperAdmin", false );

  // Restricts routes to authenticated users.
  instance.decorate( "isAuth", isAuth );

  // Restricts routes based on users access level.
  instance.decorate( "restrict", restrict );

  next();
});