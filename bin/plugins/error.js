const fastifyPlugin = require( "fastify-plugin" );

const handleError = ( err, msg ) => {
  err.clientmsg = msg;
  throw err;
}

// Error plugin, forwards custom message to overridden error handler.
module.exports = fastifyPlugin(( instance, options, next ) => {
  instance.setErrorHandler(( err, req, res ) => {
    instance.log.error( err );
    res.code( err.code || 500 ).send({ message: err.clientmsg || "Error" });
  });

  // Error must be thrown to global error handler to get access to "res" object.
  instance.decorate(        "handleError", handleError );
  instance.decorateRequest( "handleError", handleError );
  next();
});