const { promisify }     = require( "util" );

const fastifyPlugin     = require( "fastify-plugin" );
const jwt               = require( "jsonwebtoken" );


const sign      = promisify( jwt.sign );
const verify    = promisify( jwt.verify );
const jwtSecret = __accessEnv( "JWT_SECRET" );

const signJWT = async ( req, res, payload ) => {
  const oldToken = { ...req.token };
  delete oldToken.iat;
  delete oldToken.exp;
  const token = await sign({
    ...oldToken,
    ...payload
  }, jwtSecret, { expiresIn: "24h" });
  res.setCookie( "authorization", token, { maxAge: 1000 * 60 * 60 * 24, path: "/", httpOnly: true, sameSite: "none" });
  return token;
}

const verifyJWT = async token => {
  try {
    const decodedToken = await verify( token, jwtSecret );
    return decodedToken;
  } catch( err ){}
  return null;
}

// Plugin for decoding and signing JWT.
module.exports = fastifyPlugin(( instance, options, next ) => {
  instance.decorateRequest( "token", null );
  instance.decorateRequest( "signJWT", signJWT );
  instance.decorateRequest( "verifyJWT", verifyJWT );

  // Decode JWT on every request.
  instance.addHook( "onRequest", async ( req, res ) => {
    const token = req.cookies.authorization;
    if( !token ){
      return;
    }
    let decodedToken;
    try {
      decodedToken = await verify( token, jwtSecret );
    } catch( err ){}
    if( !decodedToken ){
      return;
    }
    req.token = decodedToken;
    return;
  });
  next();
});