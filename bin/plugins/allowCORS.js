const fastifyPlugin = require( "fastify-plugin" );

// Allows app to accept CORS requests.
module.exports = fastifyPlugin(( instance, options, next ) => {
  instance.addHook( "onRequest", ( req, res, next ) => {
    res.header( "Access-Control-Allow-Origin", "*" );                                //Allows any domain to access data (CORS).
    res.header( "Access-Control-Allow-Methods", "GET, POST, PUT, PATCH, DELETE" );   //Allows these methods for allowed origins.
    res.header( "Access-Control-Allow-Headers", "Content-Type, Authorization" );
    next();
  });
  next();
});