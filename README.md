# Back-end project

The project is microservice-like authentication module that with small modifications can be used as a standalone microservice or part of larger API.

## Requirements

If running with docker:
- Docker 18.0.0 and above
- Docker compose 1.24.0 and above

If running directly with node:
- NodeJS v14 and above
- MongoDB 4.0 and above


## Getting started

Choose in which mode you want to run the project.
Development mode supports module reloading and production mode offers better performance.

Configre relevant .env file with your own parameters if necessary.

The project comes with several ways of running it:
- Directly in localhost with NodeJS
```bash
  # Have MongoDB installed locally and provide connection url to development.env.
  # Install packages
  npm ci

  # Start the server
  npm run dev
```
- Directly in localhost with Docker (*RECOMMENDED*)
```bash 
  docker-compose -f ./docker-compose.dev.yml up --build
```
- Production mode typically on server with Docker
```bash
  docker-compose up --build
```

## Watch video and import postman configs

Video demonstrating functionality of the application, export of postman configuration used in video and learning diary is available [here](https://drive.google.com/drive/folders/1BSksACjhy89qztoLEjv4dWP8l3o8pbcA?usp=sharing).

To import postman configuration, open postman and click import in top-left of the window.

## API docs

[Here](docs/README.md)

## Note!

First start-up may take a while due to docker images being created.

To reclaim space used by leftover containers after finishing with the project, run
```bash
  docker system prune -a
```

Usually .env files should not be located within the procect! They are included here for easy testability. In ideal setup they could sit one level above the root folder outside of source control along with docker-compose files running multiple services or be provided by CI/CD pipeline.