FROM node:14-alpine AS builder

WORKDIR /var/www/backend
RUN apk add --update --no-cache curl bash python3 make g++
COPY ./package*.json ./
RUN npm ci --quiet --unsafe-perm
COPY ./ ./
RUN npm prune --production \
    && curl -sfL https://install.goreleaser.com/github.com/tj/node-prune.sh | bash -s -- -b /usr/local/bin \
    && /usr/local/bin/node-prune


FROM node:14-alpine

WORKDIR /var/www/backend
COPY --from=builder /var/www/backend .
EXPOSE 9012
CMD ["npm", "start"]
