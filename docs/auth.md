[Back](README.md)

This service contains authentication functionality and can act alone as a simple login/logout service if there is no need for complex user objects.

Accounts can have one of the following roles:
- regular
- editor
- admin
- super admin


User objects are of following form
```js
  // Create and update user with object like this.
  {
    "name": "name",
    "email": "asd@asd.asd",
    "role": "admin",          // Default: regular
    "password": "pwd",
    "confirmPassword": "pwd"  // Only when user is registering. Must equal password for register to succeed.
  }
```
### API

| Method | URI                                      | Description
| ------ | ---------------------------------------- | -----------------------------
| POST   | /v1/auth/register                        | Registers new user
| POST   | /v1/auth/login                           | Logs in
| POST   | /v1/auth/logout                          | Logs out
| POST   | /v1/auth/verify                          | Verifies token