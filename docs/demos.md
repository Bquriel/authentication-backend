[Back](README.md)

This service has functionally nothig to do with the project and exists as a easy way to quickly test acceess rights.  
After successfully login the JWT token will be saved in "authorization" cookie for the sake of testing and it should be passed to these routes.  
For real use the token is returned along with user object in response body.  
In case on monolithic API using cookie would be fine if CSRF token was implemented as well.

### API

| Method | URI                                      | Description
| ------ | ---------------------------------------- | -----------------------------
| GET    | /v1/demos/access-auth                    | Test route restricted to only logged users
| GET    | /v1/demos/access-regular                 | Test route restricted to regular users and above
| GET    | /v1/demos/access-editor                  | Test route restricted to editor users and above
| GET    | /v1/demos/access-admin                   | Test route restricted to admin users and above
| GET    | /v1/demos/access-super-admin             | Test route restricted to super admins