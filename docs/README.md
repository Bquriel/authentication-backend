[Back](../README.md)

# API documentation

## Structure

The API consists of three services:

- [Authentication](auth.md)
- [Users](users.md)
- [Demos](demos.md) for testing the project