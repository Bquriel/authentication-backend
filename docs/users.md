[Back](README.md)

This service is for basic CRUD controls for users.

To interact with users one must be of role *editor* or above. In majority of apps, only Super Admin/Admin should be able to create/update/delete users.

### API

| Method | URI                                      | Description
| ------ | ---------------------------------------- | -----------------------------
| GET    | /v1/users                                | Get all users
| GET    | /v1/users/:userId                        | Get one user
| POST   | /v1/users                                | Adds user
| PUT    | /v1/users                                | Edits user
| DELETE | /v1/users                                | Deletes user